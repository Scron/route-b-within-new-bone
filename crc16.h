#ifndef _CRC16_H_
#define _CRC16_H_

uint16_t crc16_update(uint16_t crc, uint8_t a);

#endif
