#ifndef _WORD_H_
#define _WORD_H_

inline uint8_t lowByte(uint16_t input);
inline uint8_t highByte(uint16_t input);

#endif

