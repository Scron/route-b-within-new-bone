#ifndef _MODBUS_H_
#define _MODBUS_H_

# define  MAXBUFFERSIZE 256

# define READHOLDINGREGISTERS       0x03       ///< Modbus function 0x03 Read Holding Registers
# define READINPUTREGISTERS         0x04       ///< Modbus function 0x04 Read Input Registers
# define WRITESINGLEREGISTER        0x06       ///< Modbus function 0x06 Write Single Register
# define WRITEMULTIPLEREGISTERS     0x10       ///< Modbus function 0x10 Write Multiple Registers

# define Date_MMDD  4096

uint16_t crc16_update(uint16_t crc, uint8_t a);
int initModbus(void);
void creatModbusADU(uint8_t SlaveID,uint8_t FunctionCode,uint16_t Address,uint16_t Qty,uint16_t *buf);
void sendModbusAPU(void);
int readModbusAPU(uint8_t *buf);

extern int uartFD;

#endif 

