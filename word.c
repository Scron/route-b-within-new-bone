#include "main.h"
#include "word.h"

inline uint8_t lowByte(uint16_t input)
{
  return (uint8_t) ((input) & 0xFF);
}

inline uint8_t highByte(uint16_t input)
{
  return (uint8_t) ((input & 0xFF00) >> 8);
}


