TARGET = client

CC = cc

CFLAGS = -Wall -g

INCLUDES = -I libmqtt

LIBS = libmqtt/*.c -lcurl -lpthread -lcrypt -lrt -lm

SOURCES = client.c

test.o: crc16.o main.o Modbus.o SCB.o word.o wiringSerial.o
	${CC} ${CFLAGS} ${INCLUDES} ${LIBS} crc16.o main.o Modbus.o SCB.o wiringSerial.o word.o -o test.out
crc16.o: crc16.c
	gcc crc16.c -c
main.o: main.c
	gcc main.c -c
Modbus.o: Modbus.c
	gcc Modbus.c -c
SCB.o: SCB.c
	gcc SCB.c -c
wiringSerial.o: wiringSerial.c
	gcc wiringSerial.c -c
word.o: word.c
	gcc word.c -c
clean:
	rm -fr *.o
