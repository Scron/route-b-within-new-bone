#ifndef _SCB_H_
#define _SCB_H_

#define  Va            4096
#define  Date_MMDD     4096

typedef union {
    struct {
        unsigned char month;
        unsigned char date;
        unsigned char hour;
        unsigned char min;
        unsigned char year;
        unsigned char sec;
    };    
    unsigned char array[6];
} SCB_time_struct;
     
char getSCBDate(uint8_t SlaveID);
char setSCBDate(uint8_t SlaveID,uint8_t year,uint8_t month,uint8_t date,uint8_t hour,uint8_t min,uint8_t sec);

extern SCB_time_struct SCBtime;

#endif

