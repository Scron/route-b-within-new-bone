#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <termios.h>
#include <signal.h>
#include <unistd.h>

#include "wiringSerial.h"
#include "main.h"
#include "word.h"
#include "Modbus.h" 
#include "crc16.h"

int uartFD = 0;

uint8_t  ModbusADU[256],
         ModbusADUSize = 0,
         ModBusSlave = 21;

int initModbus(void)
{
    struct termios options;
 
    uartFD = serialOpen ("/dev/ttyRPC0", 9600);
    if(uartFD < 0)
    {
        perror("Error");
        return FAILURE;
    }
    if(tcgetattr(uartFD,&options)!= 0)
    {
        goto ERR_OCURR;
    }

    options.c_cc[VTIME] = 2; // Default timeout is 15 seconds
                             // change to 0.2 second
    if(tcsetattr(uartFD,TCSANOW,&options) != 0)
    {
ERR_OCURR:
        perror("Setting timeout error");
        return FAILURE;
    }

    return SUCCES;
}

void creatModbusADU(uint8_t SlaveID,uint8_t FunctionCode,uint16_t Address,uint16_t Qty,uint16_t *buf)
{
     int i = 0;
     uint16_t u16CRC = 0xFFFF;
     
     ModbusADU[ModbusADUSize++] = SlaveID;
     ModbusADU[ModbusADUSize++] = FunctionCode;
     
     switch(FunctionCode)
     {
        	case READINPUTREGISTERS:
        	case READHOLDINGREGISTERS:
              ModbusADU[ModbusADUSize++] = highByte(Address);
              ModbusADU[ModbusADUSize++] = lowByte(Address);
              ModbusADU[ModbusADUSize++] = highByte(Qty);
              ModbusADU[ModbusADUSize++] = lowByte(Qty);
        	break;

        	case WRITESINGLEREGISTER:
        	case WRITEMULTIPLEREGISTERS:
              ModbusADU[ModbusADUSize++] = highByte(Address);
              ModbusADU[ModbusADUSize++] = lowByte(Address);
         break;
     }

     switch(FunctionCode)
     {
         case WRITESINGLEREGISTER:
        	     ModbusADU[ModbusADUSize++] = highByte(*buf);
              ModbusADU[ModbusADUSize++] = lowByte(*buf);
         break;

         case WRITEMULTIPLEREGISTERS:
              ModbusADU[ModbusADUSize++] = highByte(Qty);
              ModbusADU[ModbusADUSize++] = lowByte(Qty);
              ModbusADU[ModbusADUSize++] = lowByte(Qty << 1);

              for (i = 0; i < Qty; i++)
              {
                ModbusADU[ModbusADUSize++] = highByte(buf[i]);
                ModbusADU[ModbusADUSize++] = lowByte(buf[i]);
              }
         break;
     }

     // append CRC
     for (i = 0; i < ModbusADUSize; i++)
     {
         u16CRC = crc16_update(u16CRC, ModbusADU[i]);
     }
     ModbusADU[ModbusADUSize++] = lowByte(u16CRC);
     ModbusADU[ModbusADUSize++] = highByte(u16CRC);
#ifdef _DEBUG_
     printf("we will send this package\n");
     printf("length:%3d\n",ModbusADUSize);
     for (i = 0; i < ModbusADUSize; i++)
     {
         printf("%.2X\n",ModbusADU[i]);
     }
     printf("-------------------------\n");
#endif
}

void sendModbusAPU(void)
{
     int i = 0;
     for(i = 0;i < ModbusADUSize;i++){
        serialPutchar (uartFD, ModbusADU[i]) ;
     }     
     memset(ModbusADU,0,ModbusADUSize);
     ModbusADUSize = 0;
}

/* The array which the pointer(@para :buf)
 * point to should be 256 bytes, because of the
 * max modbus ADU size is 256 bytes.
 */
int readModbusAPU(uint8_t *buf)
{
     int i = 0,cnt = 0,temp = 0;
     uint16_t u16CRC = 0xFFFF;
     
     do{
        buf[cnt++] = serialGetchar(uartFD);
        if(serialDataAvail(uartFD) > 0){
            continue;
        }else{
            usleep(8000);
        }
     }while(serialDataAvail(uartFD) > 0);
     
#ifdef _DEBUG_
     printf("What we got:\n");
     for(i = 0; i < cnt; i++)
     {
           printf("%.2X\n",buf[i]);
     }
     printf("------------\n");
#endif
     /* Check the 16 bits CRC */
     for (i = 0; i < cnt-2; i++)
     {
         u16CRC = crc16_update(u16CRC, buf[i]);
     }
     if(buf[i] == lowByte(u16CRC)&& buf[i+1] == highByte(u16CRC))
     {
         return cnt;
     }
     return FAILURE;
}


