#include <stddef.h>
#include <stdio.h>
#include "main.h"
#include "Modbus.h"
#include "SCB.h"

SCB_time_struct SCBtime;

char getSCBDate(uint8_t SlaveID)
{
     int temp = 0;
     uint8_t rxbuf[12] = {0};
     uint16_t *unuse = NULL;
 
     creatModbusADU(SlaveID,READHOLDINGREGISTERS,Date_MMDD,3,unuse);
     sendModbusAPU();
     temp = readModbusAPU(rxbuf);
     if(temp == FAILURE && rxbuf[1] & 0x80)
     {
         return FAILURE;
     }else{
         SCBtime.month = rxbuf[3];
         SCBtime.date  = rxbuf[4];
         SCBtime.hour  = rxbuf[5];
         SCBtime.min   = rxbuf[6];
         SCBtime.year  = rxbuf[7];
         SCBtime.sec   = rxbuf[8];
     }
#ifdef _DEBUG_
    int i = 0;
    printf("\nDate data is :\n");
    for(i = 0;i < 6;i++){
        printf("%X\n",SCBtime.array[i]);
    }
    printf("----------------\n\n");
#endif
     return SUCCES;
 }
         
char setSCBDate(uint8_t SlaveID,uint8_t year,uint8_t month,uint8_t date,uint8_t hour,uint8_t min,uint8_t sec)
{
     int temp = 0;
     uint8_t  rxbuf[8] = {0};
     uint16_t buf[3] = {0};

     buf[0] = ((uint16_t)month << 8) | ((uint16_t)date & 0x00FF);
     buf[1] = ((uint16_t)hour  << 8) | ((uint16_t)min  & 0x00FF);
     buf[2] = ((uint16_t)year  << 8) | ((uint16_t)sec  & 0x00FF);
 
     creatModbusADU(SlaveID,WRITEMULTIPLEREGISTERS,Date_MMDD,3,buf);
     sendModbusAPU();
     temp = readModbusAPU(rxbuf);
     if(temp == FAILURE && rxbuf[1] & 0x80)
     {
         return FAILURE;
     }
#ifdef _DEBUG_
    int i = 0;
    for(i = 0;i < 6;i++){
        printf("%X\n",SCBtime.array[i]);
    }
#endif
     return SUCCES;
 }

